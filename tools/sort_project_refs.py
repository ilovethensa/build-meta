#!/usr/bin/env python3
import sys
import pathlib
from ruamel.yaml import YAML
# NOTE: ruamel.yaml is a dependency of bst, so we can use it freely

path = pathlib.Path("project.refs")
yaml = YAML()

refs = yaml.load(path)

refs["projects"]["carbonOS"] = dict(sorted(refs["projects"]["carbonOS"].items()))

yaml.dump(refs, path)
