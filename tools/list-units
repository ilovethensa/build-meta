#!/usr/bin/env bash
# USAGE: tools/list-units MATCH-PATTERN...
#
# Valid match patterns:
# all           - all units
# BOARD         - all variants of given board
# @ARCH         - all units of given architecture
# BOARD/VARIANT - single specific unit

set -o pipefail

function die {
	echo "$@" 1>&2
	exit 1
}

function print_boards {
	for dir in boards/*/; do
		board=$(basename $dir)

		source boards/$board/BOARD
		[ -n "$1" -a "$1" != "$ARCH" ] && continue
		
		print_variants $board
	done
}

function print_variants {
	[ -d "boards/$1" ] || die "Invalid board: $1"
	source boards/$1/BOARD
	for variant in "${VARIANTS[@]}"; do
		echo "$1/$variant"
	done
}

function verify_unit {
	board="${1%/*}"
	variant="${1#*/}"

	[ -d "boards/$board" ] || die "Invalid board: $board"
	source boards/$board/BOARD
	for v in "${VARIANTS[@]}"; do
		if [ $v == $variant ]; then
			echo "$board/$variant"
			return
		fi
	done
	die "Invalid variant: $variant"
}

for pattern in "$@"; do
	case $pattern in
		all) print_boards ;;
		@*) print_boards ${pattern#@} ;;
		*/*) verify_unit $pattern ;;
		*) print_variants $pattern ;;
	esac
done | awk '!seen[$0]++'
