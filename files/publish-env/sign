#!/usr/bin/env bash
# Script that signs parts of carbonOS
#
# Expected input:
# - keys/sign/ at /keys
# - keys/$BOARD/$VARIANT/ at /unit-keys
#
# This script generates the following files in /work:
# - boot.efi: systemd-boot, signed for secure boot
# - uki.efi: carbonOS UKI, signed for secure boot
# - os.img: squashfs of /usr + signed binaries
# - verity.img: dm-verity Merkel tree
# - roothash.txt: dm-verity root hash
# - verity-sig.img: usr-verity-signature partition
# - os-release: The os-release metadata from the image
#
# There are some EFI binaries that need to be embedded into the
# OS. Specifically: systemd-boot and fwupd. Since these binaries will
# eventually make their way into the ESP, they need to be signed
# for secure boot.

function die {
	echo "$@" 1>&2
	exit 1
}

set -eo pipefail
export SOURCE_DATE_EPOCH=$(date --date="$2" +%s)

# Verify inputs
[ -d /work ] || die "Missing work dir"
[ -d /keys ] || die "Missing signing keys"

# Prepare keys
if [ -f /keys/privkey.pkcs11 ]; then
	PRIVKEY=$(cat /keys/privkey.pkcs11)
	SBSIGN_ENGINE="--engine pkcs11"
	OPENSSL_ENGINE="-engine pkcs11"
	UKIFY_ENGINE="--signing-engine=pkcs11"
else
	PRIVKEY=/keys/privkey.pem
	SBSIGN_ENGINE=""
	OPENSSL_ENGINE=""
	UKIFY_ENGINE=""
fi

# Extract os-release
tar -xOf /usr.tar lib/os-release > /work/os-release

# Sign systemd-boot
echo "[SIGN] Signing systemd-boot..."
SD_BOOT=$(realpath /usr/lib/systemd/boot/efi/systemd-boot*.efi)
SD_BOOT_DEST=/tmp/inject/${SD_BOOT#/usr}
sbsign $SBSIGN_ENGINE --key $PRIVKEY --cert /keys/pubkey.cert \
	--output /work/boot.efi $SD_BOOT
install -Dm644 /work/boot.efi /tmp/inject/${SD_BOOT#/usr/}.signed

# Sign fwupd
echo "[SIGN] Signing fwupd..."
FWUPD=$(realpath /usr/libexec/fwupd/efi/fwupd*.efi)
sbsign $SBSIGN_ENGINE --key $PRIVKEY --cert /keys/pubkey.cert \
	--output /tmp/fwupd.efi $FWUPD
install -Dm644 /tmp/fwupd.efi /tmp/inject/${FWUPD#/usr/}.signed

# Sign kernel modules
echo "[SIGN] Signing kernel modules..."
# TODO: Figure out how to use ephemeral signing key
cp /linux /tmp/linux
cp -r /usr/lib/modules /tmp/inject/lib/modules
# TODO: openssl req -new -nodes -utf8 -sha512 -days 1 \
#		-batch -x509 -config /usr/src/linux/certs/x509.genkey \
#		-outform PEM -out /tmp/kernel.cert -keyout /tmp/kernel.pem \
#		2>/dev/null || die "Failed to generate kmod signing key"
function kmod_sign { # $1 = $PRIVKEY, $2 = path to module
	/usr/src/linux/scripts/sign-file sha512 $1 /keys/pubkey.cert $2 || exit 255
	zstd -q -19 --rm $2 || exit 255
}
export -f kmod_sign
find /tmp/inject/lib/modules -name '*.ko' | \
	xargs -n1 -P$MAXJOBS bash -c 'kmod_sign "$@"' _ "$PRIVKEY"
# TODO: /usr/src/linux/scripts/insert-sys-cert -b /tmp/linux -c /tmp/kernel.cert
depmod --all --basedir /tmp/inject $(cat /linux.ver)
dracut /tmp/initrd-kmods.img $(cat /linux.ver) --kernel-only \
	--kmoddir=/tmp/inject/lib/modules/$(cat /linux.ver) \
	--fwdir=/usr/lib/firmware --no-early-microcode \
	--nostrip --quiet

# Inject signed EFI binaries into os.img
echo "[SIGN] Injecting signed binaries into os.img..."
(cd /tmp/inject && bsdtar -c -f- * @/usr.tar) | \
	sqfstar -quiet -comp zstd -no-progress -all-root /work/os.img

# Generate dm-verity
echo "[SIGN] Generating dm-verity..."
veritysetup format /work/os.img /work/verity.img \
	--root-hash-file /work/roothash.txt > /dev/null

# Generate the verity-signature partition
echo "[SIGN] Generating usr-verity-sig partition..."
openssl cms -sign $OPENSSL_ENGINE -inkey $PRIVKEY -signer /keys/pubkey.cert \
	-binary -in /work/roothash.txt -outform der -out /tmp/roothash.pkcs7
openssl x509 -in /keys/pubkey.cert -outform der | sha256sum | \
	awk '{print $1}' > /tmp/cert-fprint
cat <<EOF | tr -d "[:space:]" > /work/verity-sig.img
{
	"rootHash": "$(cat /work/roothash.txt)",
	"signature": "$(base64 -w 0 /tmp/roothash.pkcs7)",
	"certificateFingerprint": "$(cat /tmp/cert-fprint)"
}
EOF

# Prepare signing keys for PCRSIG
if [ -f /unit-keys/decrypt.gpg ]; then
	export GNUPGHOME="$1"
	gpg --quiet --import /unit-keys/decrypt.gpg
	gpg --decrypt /unit-keys/privkey.pem -o /tmp/pcrsig-privkey.pem
	PCRSIG_PRIV="/tmp/pcrsig-privkey.pem"
else
	PCRSIG_PRIV="/unit-keys/privkey.pem"
fi

# Generate & sign a UKI
echo "[SIGN] Generating UKI..."
UKI_SECTIONS=(
	--linux=/tmp/linux
	--initrd=/initramfs.img
	--initrd=/tmp/initrd-kmods.img
	--cmdline="$(cat /cmdline.txt /work/roothash.txt)"
	--os-release=@/work/os-release
	--uname=$(cat /linux.ver)
)
[ -f /devicetree.dtb ] && UKI_SECTIONS+=(--devicetree /devicetree.dtb)
[ -f /splash.bmp ] && UKI_SECTIONS+=(--splash /splash.bmp)
UKI_SECUREBOOT=(
	--secureboot-certificate=/keys/pubkey.cert
	$UKIFY_ENGINE
	--secureboot-private-key=$PRIVKEY
	--sign-kernel
)
UKI_PCRSIG_INITRD=(
	--phases="enter-initrd"
	--pcr-private-key=$PCRSIG_PRIV
	--pcr-public-key=/unit-keys/pubkey.pem
)
ukify build \
	"${UKI_SECTIONS[@]}" \
	"${UKI_SECUREBOOT[@]}" \
	"${UKI_PCRSIG_INITRD[@]}" \
	--output=/work/uki.efi >/dev/null
