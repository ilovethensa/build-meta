#!/usr/bin/env bash
# Places all generated binaries into the remote
# repo format that is consumed by sysupdate
#
# Expected input
# - Output of /sign at /work
# - Output of /mkddi at /work
# - Repo GPG keys (keys/repo) at /keys
# - Local publishing directory at /pub
#
# Files generated:
# ddi.img.xz -> /pub/img/$VERSION/carbonOS_$VERSION_$BOARD_VARIANT.img.gz
# os.img -> /pub/updates/v1/$BOARD/$VARIANT/usr_$VERSION_$UUID
# verity.img -> /pub/updates/.../verity_$VERSION_$UUID
# verity-sig.img -> /pub/updates/.../verity_sig_$VERSION
# uki.efi -> /pub/updates/.../uki_$VERSION
# SHA256SUMS is updated with new files
# SHA256SUMS.gpg is updated

export GNUPGHOME="$1"

function die {
	echo "$@" 1>&2
	exit 1
}

set -eo pipefail

# Verify inputs
[ -d /work ] || die "Missing workdir"
[ -d /keys ] || die "Missing repo keys"
[ -d /pub ] || die "Missing publish destination"

# Collect info about the image
source /work/os-release
VERSION=$IMAGE_VERSION
BOARD=$IMAGE_ID
VARIANT=$VARIANT_ID
function to_uuid {
	sed 's/..../& /g' | awk '{OFS="-"; print $1$2,$3,$4,$5,$6$7$8}'
}
USR_UUID=$(head -c 32 /work/roothash.txt | to_uuid)
VERITY_UUID=$(tail -c 32 /work/roothash.txt | to_uuid)

UPD_DIR=updates/v1/$BOARD/$VARIANT
IMG_DIR=img/$VERSION

# Install the DDI
echo "[PUBLISH] Preparing DDI..."
mkdir -p /tmp/$IMG_DIR && cd /tmp/$IMG_DIR

cp /work/ddi.img.xz carbonOS_${VERSION}_${BOARD}_${VARIANT}.img.xz

# Install the updates
echo "[PUBLISH] Preparing updates..."
mkdir -p /tmp/$UPD_DIR && cd /tmp/$UPD_DIR

cp /work/os.img usr_${VERSION}_${USR_UUID}
cp /work/verity.img verity_${VERSION}_${VERITY_UUID}
cp /work/verity-sig.img verity_sig_${VERSION}
cp /work/uki.efi uki_${VERSION}

# Copy everything over to /pub
echo "[PUBLISH] Exporting to local repo..."
cp -r /tmp/{updates,img} /pub

# Update checksums
echo "[PUBLISH] Updating SHA256SUMS..."
gpg --quiet --import /keys/pubkey.gpg
[ -f /keys/privkey.gpg ] && gpg --quiet --import /keys/privkey.gpg || true

function gen_sha256sums {
	# We do this in a specific way: we want the latest version
	# to be on the top of the file, so sysupdate finds it first,
	# but we also want to deduplicate lines in case tools/publish
	# is re-run on the same input file.

	temp=$(mktemp)

	{
	    cd /tmp/$1
		sha256sum *
		[ -f /pub/$1/SHA256SUMS ] && cat /pub/$1/SHA256SUMS || true
	} | awk '!seen[$2]++' > $temp
	# awk script: dedup by file name, keep first seen
	# Reference: https://stackoverflow.com/a/1444448

	cat $temp > /pub/$1/SHA256SUMS
	unlink $temp

	gpg --detach-sign --batch --yes -o /pub/$1/SHA256SUMS.gpg /pub/$1/SHA256SUMS
}

gen_sha256sums $UPD_DIR

(
	# /pub/$IMG_DIR/SHA256SUMS is the only file shared among multiple
	# instances of this script executing in parallel (so long as they
	# are executing for unique units). Thus, we need to synchronize to
	# avoid losing writes to this file

	# Lock the file
	touch /pub/$IMG_DIR/SHA256SUMS
	exec {lock}< /pub/$IMG_DIR/SHA256SUMS
	flock -e $lock

	# Generate the sha256sums
	gen_sha256sums $IMG_DIR

	# Automatically unlocks when subshell exits
)
