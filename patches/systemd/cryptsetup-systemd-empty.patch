From 6f0c5c591edfa01a2f26f32cd789048abbaaa5e0 Mon Sep 17 00:00:00 2001
From: Adrian Vovk <adrianvovk@gmail.com>
Date: Fri, 16 Jun 2023 13:52:50 -0400
Subject: [PATCH 1/4] cryptenroll: Add a way to enroll an empty slot

An empty slot can be used to (temporarily) disable encryption on a
volume
without actually decrypting it.

Unlike an empty password, this enrolls some JSON data so that tools like
systemd-cryptsetup can discover the availability of an empty slot.
---
 man/systemd-cryptenroll.xml         | 11 ++++-
 src/cryptenroll/cryptenroll-empty.c | 65 +++++++++++++++++++++++++++++
 src/cryptenroll/cryptenroll-empty.h |  8 ++++
 src/cryptenroll/cryptenroll.c       | 18 ++++++++
 src/cryptenroll/cryptenroll.h       |  1 +
 src/cryptenroll/meson.build         |  1 +
 6 files changed, 103 insertions(+), 1 deletion(-)
 create mode 100644 src/cryptenroll/cryptenroll-empty.c
 create mode 100644 src/cryptenroll/cryptenroll-empty.h

diff --git a/man/systemd-cryptenroll.xml b/man/systemd-cryptenroll.xml
index 6ca850e0ef24..526d6df798e1 100644
--- a/man/systemd-cryptenroll.xml
+++ b/man/systemd-cryptenroll.xml
@@ -256,6 +256,14 @@
         </para></listitem>
       </varlistentry>
 
+      <varlistentry>
+        <term><option>--empty</option></term>
+
+        <listitem><para>Enroll an empty key. This has the effect of disabling encryption on the volume
+        without decrypting it. Unlike an empty <option>--password</option>, a key enrolled via this
+        option can automatically unlock the volume with no performance penalty.</para></listitem>
+      </varlistentry>
+
       <varlistentry>
         <term><option>--unlock-key-file=</option><replaceable>PATH</replaceable></term>
 
@@ -463,7 +471,8 @@
 
         <listitem><para>Wipes one or more LUKS2 key slots. Takes a comma separated list of numeric slot
         indexes, or the special strings <literal>all</literal> (for wiping all key slots),
-        <literal>empty</literal> (for wiping all key slots that are unlocked by an empty passphrase),
+        <literal>empty</literal> (for wiping all key slots that are unlocked by an empty passphrase; not
+        just those enrolled via <option>--empty</option>),
         <literal>password</literal> (for wiping all key slots that are unlocked by a traditional passphrase),
         <literal>recovery</literal> (for wiping all key slots that are unlocked by a recovery key),
         <literal>pkcs11</literal> (for wiping all key slots that are unlocked by a PKCS#11 token),
diff --git a/src/cryptenroll/cryptenroll-empty.c b/src/cryptenroll/cryptenroll-empty.c
new file mode 100644
index 000000000000..1f97d10c2796
--- /dev/null
+++ b/src/cryptenroll/cryptenroll-empty.c
@@ -0,0 +1,65 @@
+/* SPDX-License-Identifier: LGPL-2.1-or-later */
+
+#include "cryptenroll-empty.h"
+#include "json.h"
+
+int enroll_empty(
+                struct crypt_device *cd,
+                const void *volume_key,
+                size_t volume_key_size) {
+        _cleanup_(json_variant_unrefp) JsonVariant *v = NULL;
+        _cleanup_free_ char *keyslot_as_string = NULL;
+        const char *node;
+        int keyslot, r, q;
+
+        assert_se(cd);
+        assert_se(volume_key);
+        assert_se(volume_key_size > 0);
+
+        assert_se(node = crypt_get_device_name(cd));
+
+        /* No need to robustly protect against brute-force attacks... */
+        r = cryptsetup_set_minimal_pbkdf(cd);
+        if (r < 0)
+                return log_error_errno(r, "Failed to set minimal PBKDF: %m");
+
+        keyslot = crypt_keyslot_add_by_volume_key(
+                        cd,
+                        CRYPT_ANY_SLOT,
+                        volume_key,
+                        volume_key_size,
+                        /* passphrase= */ "",
+                        /* passphrase_size= */ 0);
+        if (keyslot < 0)
+                return log_error_errno(keyslot, "Failed to add empty kesy to %s: %m", node);
+
+        if (asprintf(&keyslot_as_string, "%i", keyslot) < 0) {
+                r = log_oom();
+                goto rollback;
+        }
+
+        r = json_build(&v,
+                       JSON_BUILD_OBJECT(
+                                       JSON_BUILD_PAIR("type", JSON_BUILD_CONST_STRING("systemd-empty")),
+                                       JSON_BUILD_PAIR("keyslots", JSON_BUILD_ARRAY(JSON_BUILD_STRING(keyslot_as_string)))));
+        if (r < 0) {
+                log_error_errno(r, "Failed to prepare empty key JSON token object: %m");
+                goto rollback;
+        }
+
+        r = cryptsetup_add_token_json(cd, v);
+        if (r < 0) {
+                log_error_errno(r, "Failed to add empty JSON token to LUKS2 header: %m");
+                goto rollback;
+        }
+
+        log_info("Empty key enrolled as key slot %i.", keyslot);
+        return keyslot;
+
+rollback:
+        q = crypt_keyslot_destroy(cd, keyslot);
+        if (q < 0)
+                log_debug_errno(q, "Unable to remove key slot we just added, can't rollback, sorry: %m");
+
+        return r;
+}
diff --git a/src/cryptenroll/cryptenroll-empty.h b/src/cryptenroll/cryptenroll-empty.h
new file mode 100644
index 000000000000..2ada65f071c5
--- /dev/null
+++ b/src/cryptenroll/cryptenroll-empty.h
@@ -0,0 +1,8 @@
+/* SPDX-License-Identifier: LGPL-2.1-or-later */
+#pragma once
+
+#include <sys/types.h>
+
+#include "cryptsetup-util.h"
+
+int enroll_empty(struct crypt_device *cd, const void *volume_key, size_t volume_key_size);
diff --git a/src/cryptenroll/cryptenroll.c b/src/cryptenroll/cryptenroll.c
index fe25619d858d..7fda7f1a756f 100644
--- a/src/cryptenroll/cryptenroll.c
+++ b/src/cryptenroll/cryptenroll.c
@@ -4,6 +4,7 @@
 
 #include "ask-password-api.h"
 #include "build.h"
+#include "cryptenroll-empty.h"
 #include "cryptenroll-fido2.h"
 #include "cryptenroll-list.h"
 #include "cryptenroll-password.h"
@@ -78,6 +79,7 @@ static const char* const enroll_type_table[_ENROLL_TYPE_MAX] = {
         [ENROLL_PKCS11] = "pkcs11",
         [ENROLL_FIDO2] = "fido2",
         [ENROLL_TPM2] = "tpm2",
+        [ENROLL_EMPTY] = "empty",
 };
 
 DEFINE_STRING_TABLE_LOOKUP(enroll_type, EnrollType);
@@ -88,6 +90,7 @@ static const char *const luks2_token_type_table[_ENROLL_TYPE_MAX] = {
         [ENROLL_PKCS11] = "systemd-pkcs11",
         [ENROLL_FIDO2] = "systemd-fido2",
         [ENROLL_TPM2] = "systemd-tpm2",
+        [ENROLL_EMPTY] = "systemd-empty",
 };
 
 DEFINE_STRING_TABLE_LOOKUP(luks2_token_type, EnrollType);
@@ -106,6 +109,7 @@ static int help(void) {
                "     --version         Show package version\n"
                "     --password        Enroll a user-supplied password\n"
                "     --recovery-key    Enroll a recovery key\n"
+               "     --empty           Enroll an empty key\n"
                "     --unlock-key-file=PATH\n"
                "                       Use a file to unlock the volume\n"
                "     --unlock-fido2-device=PATH\n"
@@ -167,6 +171,7 @@ static int parse_argv(int argc, char *argv[]) {
                 ARG_FIDO2_WITH_UP,
                 ARG_FIDO2_WITH_UV,
                 ARG_FIDO2_CRED_ALG,
+                ARG_EMPTY,
         };
 
         static const struct option options[] = {
@@ -174,6 +179,7 @@ static int parse_argv(int argc, char *argv[]) {
                 { "version",                      no_argument,       NULL, ARG_VERSION               },
                 { "password",                     no_argument,       NULL, ARG_PASSWORD              },
                 { "recovery-key",                 no_argument,       NULL, ARG_RECOVERY_KEY          },
+                { "empty",                        no_argument,       NULL, ARG_EMPTY                 },
                 { "unlock-key-file",              required_argument, NULL, ARG_UNLOCK_KEYFILE        },
                 { "unlock-fido2-device",          required_argument, NULL, ARG_UNLOCK_FIDO2_DEVICE   },
                 { "pkcs11-token-uri",             required_argument, NULL, ARG_PKCS11_TOKEN_URI      },
@@ -247,6 +253,14 @@ static int parse_argv(int argc, char *argv[]) {
                         arg_enroll_type = ENROLL_RECOVERY;
                         break;
 
+                case ARG_EMPTY:
+                        if (arg_enroll_type >= 0)
+                                return log_error_errno(SYNTHETIC_ERRNO(EINVAL),
+                                                       "Multiple operations specified at once, refusing.");
+
+                        arg_enroll_type = ENROLL_EMPTY;
+                        break;
+
                 case ARG_UNLOCK_KEYFILE:
                         if (arg_unlock_type != UNLOCK_PASSWORD)
                                 return log_error_errno(SYNTHETIC_ERRNO(EINVAL),
@@ -646,6 +660,10 @@ static int run(int argc, char *argv[]) {
                 slot = enroll_recovery(cd, vk, vks);
                 break;
 
+        case ENROLL_EMPTY:
+                slot = enroll_empty(cd, vk, vks);
+                break;
+
         case ENROLL_PKCS11:
                 slot = enroll_pkcs11(cd, vk, vks, arg_pkcs11_token_uri);
                 break;
diff --git a/src/cryptenroll/cryptenroll.h b/src/cryptenroll/cryptenroll.h
index 335d9ccd7154..fef5c11480c1 100644
--- a/src/cryptenroll/cryptenroll.h
+++ b/src/cryptenroll/cryptenroll.h
@@ -9,6 +9,7 @@ typedef enum EnrollType {
         ENROLL_PKCS11,
         ENROLL_FIDO2,
         ENROLL_TPM2,
+        ENROLL_EMPTY,
         _ENROLL_TYPE_MAX,
         _ENROLL_TYPE_INVALID = -EINVAL,
 } EnrollType;
diff --git a/src/cryptenroll/meson.build b/src/cryptenroll/meson.build
index 9080af0db9bc..43ebd3a89386 100644
--- a/src/cryptenroll/meson.build
+++ b/src/cryptenroll/meson.build
@@ -4,6 +4,7 @@ systemd_cryptenroll_sources = files(
         'cryptenroll-list.c',
         'cryptenroll-password.c',
         'cryptenroll-recovery.c',
+        'cryptenroll-empty.c',
         'cryptenroll-wipe.c',
         'cryptenroll.c',
 )

From 5195f525bf7462122ee7aaa357fa057123bcec52 Mon Sep 17 00:00:00 2001
From: Adrian Vovk <adrianvovk@gmail.com>
Date: Fri, 16 Jun 2023 13:54:37 -0400
Subject: [PATCH 2/4] repart: Mark empty LUKS password as an empty slot

This lets tools like systemd-cryptsetup automatically discover that the
slot has an empty password
---
 src/partition/repart.c | 33 ++++++++++++++++++++++++++++++---
 1 file changed, 30 insertions(+), 3 deletions(-)

diff --git a/src/partition/repart.c b/src/partition/repart.c
index 634eb2e50cc0..47785c0d98b5 100644
--- a/src/partition/repart.c
+++ b/src/partition/repart.c
@@ -3486,15 +3486,42 @@ static int partition_encrypt(Context *context, Partition *p, PartitionTarget *ta
                 return log_error_errno(r, "Failed to LUKS2 format future partition: %m");
 
         if (IN_SET(p->encrypt, ENCRYPT_KEY_FILE, ENCRYPT_KEY_FILE_TPM2)) {
-                r = sym_crypt_keyslot_add_by_volume_key(
+                int keyslot;
+
+                if (arg_key_size == 0) {
+                        /* No need for robust protection against brute-force... */
+                        r = cryptsetup_set_minimal_pbkdf(cd);
+                        if (r < 0)
+                                return log_error_errno(r, "Failed to set minimal PBKDF: %m");
+                }
+
+                keyslot = sym_crypt_keyslot_add_by_volume_key(
                                 cd,
                                 CRYPT_ANY_SLOT,
                                 NULL,
                                 VOLUME_KEY_SIZE,
                                 strempty(arg_key),
                                 arg_key_size);
-                if (r < 0)
-                        return log_error_errno(r, "Failed to add LUKS2 key: %m");
+                if (keyslot < 0)
+                        return log_error_errno(keyslot, "Failed to add LUKS2 key: %m");
+
+                if (arg_key_size == 0) {
+                        _cleanup_(json_variant_unrefp) JsonVariant *v = NULL;
+                        _cleanup_free_ char *keyslot_str;
+
+                        if (asprintf(&keyslot_str, "%i", keyslot) < 0)
+                                return log_oom();
+
+                        r = json_build(&v, JSON_BUILD_OBJECT(
+                                                JSON_BUILD_PAIR("type", JSON_BUILD_CONST_STRING("systemd-empty")),
+                                                JSON_BUILD_PAIR("keyslots", JSON_BUILD_ARRAY(JSON_BUILD_STRING(keyslot_str)))));
+                        if (r < 0)
+                                return log_error_errno(r, "Failed to prepare empty key JSON token object: %m");
+
+                        r = cryptsetup_add_token_json(cd, v);
+                        if (r < 0)
+                                return log_error_errno(r, "Failed to add empty JSON token to LUKS2 header: %m");
+                }
 
                 passphrase = strempty(arg_key);
                 passphrase_size = arg_key_size;

From 6119d2cb8ce34187b28cd0662486a8dd32d1ea78 Mon Sep 17 00:00:00 2001
From: Adrian Vovk <adrianvovk@gmail.com>
Date: Fri, 16 Jun 2023 14:51:36 -0400
Subject: [PATCH 3/4] cryptsetup: Add systemd-empty token type

This allows cryptsetup to automatically unlock any volume with an empty
key enrolled
---
 meson.build                                   | 16 +++++
 .../cryptsetup-token-systemd-empty.c          | 70 +++++++++++++++++++
 src/cryptsetup/cryptsetup-tokens/meson.build  |  4 ++
 3 files changed, 90 insertions(+)
 create mode 100644 src/cryptsetup/cryptsetup-tokens/cryptsetup-token-systemd-empty.c

diff --git a/meson.build b/meson.build
index 5d98f742e6d4..8d315c284ae9 100644
--- a/meson.build
+++ b/meson.build
@@ -2302,6 +2302,22 @@ install_libudev_static = static_library(
         pic : static_libudev_pic)
 
 if conf.get('HAVE_LIBCRYPTSETUP_PLUGINS') == 1
+        shared_library(
+                'cryptsetup-token-systemd-empty',
+                cryptsetup_token_systemd_empty_sources,
+                include_directories : includes,
+                link_args : ['-shared',
+                             '-Wl,--version-script=' + cryptsetup_token_sym_path],
+                link_with : [lib_cryptsetup_token_common,
+                             libshared],
+                dependencies : [libcryptsetup,
+                                userspace,
+                                versiondep],
+                link_depends : cryptsetup_token_sym,
+                install_rpath : rootpkglibdir,
+                install : true,
+                install_dir : libcryptsetup_plugins_dir)
+
         if conf.get('HAVE_TPM2') == 1
                 shared_library(
                         'cryptsetup-token-systemd-tpm2',
diff --git a/src/cryptsetup/cryptsetup-tokens/cryptsetup-token-systemd-empty.c b/src/cryptsetup/cryptsetup-tokens/cryptsetup-token-systemd-empty.c
new file mode 100644
index 000000000000..5829fe784674
--- /dev/null
+++ b/src/cryptsetup/cryptsetup-tokens/cryptsetup-token-systemd-empty.c
@@ -0,0 +1,70 @@
+/* SPDX-License-Identifier: LGPL-2.1-or-later */
+
+#include <errno.h>
+#include <libcryptsetup.h>
+
+#include "cryptsetup-token.h"
+#include "macro.h"
+#include "version.h"
+
+#define TOKEN_NAME "systemd-empty"
+#define TOKEN_VERSION_MAJOR "1"
+#define TOKEN_VERSION_MINOR "0"
+
+/* for libcryptsetup debug purpose */
+_public_ const char *cryptsetup_token_version(void) {
+
+        return TOKEN_VERSION_MAJOR "." TOKEN_VERSION_MINOR " systemd-v" STRINGIFY(PROJECT_VERSION) " (" GIT_VERSION ")";
+}
+
+_public_ int cryptsetup_token_open_pin(
+                struct crypt_device *cd, /* is always LUKS2 context */
+                int token /* is always >= 0 */,
+                const char *pin,
+                size_t pin_size,
+                char **ret_password, /* freed by cryptsetup_token_buffer_free */
+                size_t *ret_password_len,
+                void *usrptr /* plugin defined parameter passed to crypt_activate_by_token*() API */) {
+
+        assert(token >= 0);
+        assert(!pin || pin_size > 0);
+        assert(ret_password);
+        assert(ret_password_len);
+
+        /* free'd automatically by libcryptsetup */
+        *ret_password = strdup("");
+        *ret_password_len = 0;
+
+        return 0;
+}
+
+/*
+ * This function is called from within following libcryptsetup calls
+ * provided conditions further below are met:
+ *
+ * crypt_activate_by_token(), crypt_activate_by_token_type(type == 'systemd-empty'):
+ *
+ * - token is assigned to at least one luks2 keyslot eligible to activate LUKS2 device
+ *   (alternatively: name is set to null, flags contains CRYPT_ACTIVATE_ALLOW_UNBOUND_KEY
+ *   and token is assigned to at least single keyslot).
+ *
+ * - if plugin defines validate function (systemd-empty does not) it must have passed the
+ *   check (aka return 0)
+ */
+_public_ int cryptsetup_token_open(
+                struct crypt_device *cd, /* is always LUKS2 context */
+                int token /* is always >= 0 */,
+                char **ret_password, /* freed by cryptsetup_token_buffer_free */
+                size_t *ret_password_len,
+                void *usrptr /* plugin defined parameter passed to crypt_activate_by_token*() API */) {
+
+        return cryptsetup_token_open_pin(cd, token, NULL, 0, ret_password, ret_password_len, usrptr);
+}
+
+/*
+ * libcryptsetup callback for memory deallocation of 'password' parameter passed in
+ * any crypt_token_open_* plugin function
+ */
+_public_ void cryptsetup_token_buffer_free(void *buffer, size_t buffer_len) {
+        free(buffer);
+}
diff --git a/src/cryptsetup/cryptsetup-tokens/meson.build b/src/cryptsetup/cryptsetup-tokens/meson.build
index e7b7fbab119f..b86e57af9446 100644
--- a/src/cryptsetup/cryptsetup-tokens/meson.build
+++ b/src/cryptsetup/cryptsetup-tokens/meson.build
@@ -11,6 +11,10 @@ lib_cryptsetup_token_common = static_library(
         link_with : libshared,
         build_by_default : false)
 
+cryptsetup_token_systemd_empty_sources = files(
+        'cryptsetup-token-systemd-empty.c'
+)
+
 cryptsetup_token_systemd_tpm2_sources = files(
         'cryptsetup-token-systemd-tpm2.c',
         'luks2-tpm2.c',

From 7404a3c39a3404c181afe01cec398d9ce864e28a Mon Sep 17 00:00:00 2001
From: Adrian Vovk <adrianvovk@gmail.com>
Date: Fri, 16 Jun 2023 15:36:16 -0400
Subject: [PATCH 4/4] cryptsetup: Add test cases for empty slot

---
 test/units/testsuite-24.sh | 84 +++++++++++++++++++++++---------------
 1 file changed, 50 insertions(+), 34 deletions(-)

diff --git a/test/units/testsuite-24.sh b/test/units/testsuite-24.sh
index 391dcf999d2f..dbc96b55ebfc 100755
--- a/test/units/testsuite-24.sh
+++ b/test/units/testsuite-24.sh
@@ -81,26 +81,37 @@ WORKDIR="$(mktemp -d)"
 
 # Prepare a couple of LUKS2-encrypted disk images
 #
-# 1) Image with an empty password
-IMAGE_EMPTY="$WORKDIR/empty.img)"
-IMAGE_EMPTY_KEYFILE="$WORKDIR/empty.keyfile"
-IMAGE_EMPTY_KEYFILE_ERASE="$WORKDIR/empty-erase.keyfile"
-IMAGE_EMPTY_KEYFILE_ERASE_FAIL="$WORKDIR/empty-erase-fail.keyfile)"
-truncate -s 32M "$IMAGE_EMPTY"
-echo -n passphrase >"$IMAGE_EMPTY_KEYFILE"
-chmod 0600 "$IMAGE_EMPTY_KEYFILE"
+# 1) Image with both a password and an empty password
+IMAGE_PASS="$WORKDIR/password.img"
+IMAGE_PASS_KEYFILE="$WORKDIR/password.keyfile"
+IMAGE_PASS_KEYFILE_ERASE="$WORKDIR/password-erase.keyfile"
+IMAGE_PASS_KEYFILE_ERASE_FAIL="$WORKDIR/password-erase-fail.keyfile)"
+truncate -s 32M "$IMAGE_PASS"
+echo -n passphrase >"$IMAGE_PASS_KEYFILE"
+chmod 0600 "$IMAGE_PASS_KEYFILE"
 cryptsetup luksFormat --batch-mode \
                       --pbkdf pbkdf2 \
                       --pbkdf-force-iterations 1000 \
                       --use-urandom \
-                      "$IMAGE_EMPTY" "$IMAGE_EMPTY_KEYFILE"
-PASSWORD=passphrase NEWPASSWORD="" systemd-cryptenroll --password "$IMAGE_EMPTY"
+                      "$IMAGE_PASS" "$IMAGE_PASS_KEYFILE"
+# Enroll an empty passphrase as well
+PASSWORD=passphrase NEWPASSWORD="" systemd-cryptenroll --password "$IMAGE_PASS"
 # Duplicate the key file to test keyfile-erase as well
-cp -v "$IMAGE_EMPTY_KEYFILE" "$IMAGE_EMPTY_KEYFILE_ERASE"
+cp -v "$IMAGE_PASS_KEYFILE" "$IMAGE_PASS_KEYFILE_ERASE"
 # The key should get erased even on a failed attempt, so test that too
-cp -v "$IMAGE_EMPTY_KEYFILE" "$IMAGE_EMPTY_KEYFILE_ERASE_FAIL"
+cp -v "$IMAGE_PASS_KEYFILE" "$IMAGE_PASS_KEYFILE_ERASE_FAIL"
 
-# 2) Image with a detached header and a key file offset + size
+# 2) Image with an empty slot
+IMAGE_EMPTY="$WORKDIR/empty.img"
+truncate -s 32M "$IMAGE_EMPTY"
+cryptsetup luksFormat --batch-mode \
+                      --pbkdf pbkdf2 \
+                      --pbkdf-force-iterations 1000 \
+                      --use-urandom \
+                      "$IMAGE_EMPTY" "$IMAGE_PASS_KEYFILE"
+PASSWORD=passphrase systemd-cryptenroll --wipe-slot=password --empty "$IMAGE_EMPTY"
+
+# 3) Image with a detached header and a key file offset + size
 IMAGE_DETACHED="$WORKDIR/detached.img"
 IMAGE_DETACHED_KEYFILE="$WORKDIR/detached.keyfile"
 IMAGE_DETACHED_KEYFILE2="$WORKDIR/detached.keyfile2"
@@ -154,16 +165,19 @@ udevadm settle --timeout=30
 [[ -e /etc/crypttab ]] && cp -fv /etc/crypttab /tmp/crypttab.bak
 cat >/etc/crypttab <<EOF
 # headless should translate to headless=1
-empty_key            $IMAGE_EMPTY    $IMAGE_EMPTY_KEYFILE            headless,x-systemd.device-timeout=1m
-empty_key_erase      $IMAGE_EMPTY    $IMAGE_EMPTY_KEYFILE_ERASE      headless=1,keyfile-erase=1
-empty_key_erase_fail $IMAGE_EMPTY    $IMAGE_EMPTY_KEYFILE_ERASE_FAIL headless=1,keyfile-erase=1,keyfile-offset=4
+pass_key             $IMAGE_PASS     $IMAGE_PASS_KEYFILE             headless,x-systemd.device-timeout=1m
+pass_key_erase       $IMAGE_PASS     $IMAGE_PASS_KEYFILE_ERASE       headless=1,keyfile-erase=1
+pass_key_erase_fail  $IMAGE_PASS     $IMAGE_PASS_KEYFILE_ERASE_FAIL  headless=1,keyfile-erase=1,keyfile-offset=4
 # Empty passphrase without try-empty-password(=yes) shouldn't work
-empty_fail0          $IMAGE_EMPTY    -                               headless=1
-empty_fail1          $IMAGE_EMPTY    -                               headless=1,try-empty-password=0
-empty0               $IMAGE_EMPTY    -                               headless=1,try-empty-password
-empty1               $IMAGE_EMPTY    -                               headless=1,try-empty-password=1
-# This one expects the key to be under /{etc,run}/cryptsetup-keys.d/empty_nokey.key
-empty_nokey          $IMAGE_EMPTY    -                               headless=1
+pass_empty_fail0     $IMAGE_PASS     -                               headless=1
+pass_empty_fail1     $IMAGE_PASS     -                               headless=1,try-empty-password=0
+pass_empty0          $IMAGE_PASS     -                               headless=1,try-empty-password
+pass_empty1          $IMAGE_PASS     -                               headless=1,try-empty-password=1
+# This one expects the key to be under /{etc,run}/cryptsetup-keys.d/pass_nokey.key
+pass_nokey           $IMAGE_PASS     -                               headless=1
+
+# Unlike the empty passphrase, an image with an empty slot should automatically unlock w/o try-empty-password
+empty                $IMAGE_EMPTY    -                               headless=1
 
 detached             $IMAGE_DETACHED $IMAGE_DETACHED_KEYFILE         headless=1,header=$IMAGE_DETACHED_HEADER,keyfile-offset=32,keyfile-size=16
 detached_store0      $IMAGE_DETACHED $IMAGE_DETACHED_KEYFILE         headless=1,header=/header:LABEL=header_store,keyfile-offset=32,keyfile-size=16
@@ -191,21 +205,23 @@ mkdir -p /tmp/systemd-cryptsetup-generator.out
 systemctl daemon-reload
 systemctl list-unit-files "systemd-cryptsetup@*"
 
-cryptsetup_start_and_check empty_key
-test -e "$IMAGE_EMPTY_KEYFILE_ERASE"
-cryptsetup_start_and_check empty_key_erase
-test ! -e "$IMAGE_EMPTY_KEYFILE_ERASE"
-test -e "$IMAGE_EMPTY_KEYFILE_ERASE_FAIL"
-cryptsetup_start_and_check -f empty_key_erase_fail
-test ! -e "$IMAGE_EMPTY_KEYFILE_ERASE_FAIL"
-cryptsetup_start_and_check -f empty_fail{0..1}
-cryptsetup_start_and_check empty{0..1}
+cryptsetup_start_and_check pass_key
+test -e "$IMAGE_PASS_KEYFILE_ERASE"
+cryptsetup_start_and_check pass_key_erase
+test ! -e "$IMAGE_PASS_KEYFILE_ERASE"
+test -e "$IMAGE_PASS_KEYFILE_ERASE_FAIL"
+cryptsetup_start_and_check -f pass_key_erase_fail
+test ! -e "$IMAGE_PASS_KEYFILE_ERASE_FAIL"
+cryptsetup_start_and_check -f pass_empty_fail{0..1}
+cryptsetup_start_and_check pass_empty{0..1}
 # First, check if we correctly fail without any key
-cryptsetup_start_and_check -f empty_nokey
+cryptsetup_start_and_check -f pass_nokey
 # And now provide the key via /{etc,run}/cryptsetup-keys.d/
 mkdir -p /run/cryptsetup-keys.d
-cp "$IMAGE_EMPTY_KEYFILE" /run/cryptsetup-keys.d/empty_nokey.key
-cryptsetup_start_and_check empty_nokey
+cp "$IMAGE_PASS_KEYFILE" /run/cryptsetup-keys.d/pass_nokey.key
+cryptsetup_start_and_check pass_nokey
+
+cryptsetup_start_and_check empty
 
 cryptsetup_start_and_check detached
 cryptsetup_start_and_check detached_store{0..2}
