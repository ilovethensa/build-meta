kind: autotools

sources:
- kind: git_repo
  url: github:python/cpython.git
  track: refs/tags/v*
  exclude:
  - v*a*
  - v*b*
  - v*rc*

depends:
- pkgs/expat.bst
- pkgs/libffi.bst
- pkgs/gdbm.bst
- pkgs/xz.bst
- pkgs/sqlite.bst
- pkgs/openssl.bst

build-depends:
- buildsystems/autotools.bst

variables:
  conf-local: >-
    --with-system-expat
    --with-system-ffi
    --without-ensurepip
    --with-lto
    --enable-loadable-sqlite-extensions
    --enable-optimizations

config:
  install-commands:
    (>):
    - | # Make python3 the default python
      ln -sv python3 %{install-root}%{bindir}/python

    - | # Delete all of the unnecessary libraries
      cd %{install-root}%{libdir}/python*/
      rm -rf %{install-root}%{bindir}/idle*
      rm -rf idlelib
      rm -rf tkinter
      rm -rf turtle*
      rm -rf __pycache__/turtle.*
      rm -rf test
      rm -rf ./*/test
      rm -rf ./*/tests

public:
  bst:
    split-rules:
      devel:
        (>):
        - "%{bindir}/2to3*"
        - "%{bindir}/python*-config"
        - "%{libdir}/python*/lib2to3"
        - "%{libdir}/python*/lib2to3/**"
