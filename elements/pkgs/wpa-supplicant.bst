kind: make

sources:
- kind: git_repo
  url: w1fi:hostap.git
- kind: local
  path: files/wpa-supplicant
  directory: wpa_supplicant/carbon

# Allow legacy renegotiation with openssl 3.0 to fix connection to
# PEAP/Radius servers that don't support secure renegotiation
- kind: patch
  path: patches/wpa-supplicant/legacy-renegotiation.patch

- kind: patch
  path: patches/wpa-supplicant/lower-security-level-for-tls-1.patch
- kind: patch
  path: patches/wpa-supplicant/add-extra-ies-only-if-driver-allows.patch
- kind: patch
  path: patches/wpa-supplicant/dont-negotiate-ft-sae.patch

# Track changes to patches here: https://github.com/archlinux/svntogit-packages/blame/packages/wpa_supplicant/trunk/PKGBUILD#L46
# Last updated: Feb 1, 2023

depends:
- pkgs/openssl.bst
- pkgs/dbus.bst
- pkgs/libnl.bst
- pkgs/pcsc-lite.bst

build-depends:
- buildsystems/autotools.bst

variables:
  command-subdir: wpa_supplicant
  license-files: "README"
  make-args: >-
    BINDIR=%{bindir}
    LIBDIR=%{libdir}

config:
  configure-commands:
  - cp carbon/config .config
  install-commands:
    (>):
    - install -Dm644 systemd/*.service -t %{install-root}%{libdir}/systemd/system
    - |
      install -Dm644 dbus/fi.w1.wpa_supplicant1.service -t %{install-root}%{datadir}/dbus-1/system-services
      install -Dm644 dbus/dbus-wpa_supplicant.conf %{install-root}%{datadir}/dbus-1/system.d/wpa_supplicant.conf
