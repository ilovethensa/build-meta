kind: manual

sources:
- kind: tar # Complicated to vendor from git
  url: rust:rustc-1.71.1-src.tar.gz
# release-monitoring: 7635

depends:
- pkgs/llvm.bst
- pkgs/curl.bst
- pkgs/openssl.bst
- pkgs/libxml2.bst

build-depends:
- pkgs/rust/stage1.bst
- pkgs/python/all.bst
- pkgs/make.bst
- pkgs/git/minimal.bst
- buildsystems/cmake.bst

variables:
  rust-target: "x86_64-unknown-linux-gnu"
  llvm-target: "X86"
  config-toml: |
    [llvm]
    link-shared = true
    targets = "%{llvm-target}"
    experimental-targets = ""
    static-libstdcpp = false

    [build]
    build = "%{rust-target}"
    cargo = "%{bindir}/cargo"
    rustc = "%{bindir}/rustc"
    docs = false
    submodules = false
    locked-deps = true
    vendor = true
    extended = true
    tools = ["cargo"]
    verbose = 2

    [install]
    prefix = "%{prefix}"
    sysconfdir = "%{sysconfdir}"
    bindir = "%{bindir}"
    libdir = "%{libdir}"
    datadir = "%{datadir}"
    mandir = "%{mandir}"

    [rust]
    optimize = true
    channel = "stable"
    debuginfo-level = 2
    debuginfo-level-std = 0
    backtrace = true
    rpath = false
    default-linker = "%{bindir}/gcc"

    [target.%{rust-target}]
    llvm-config = "%{bindir}/llvm-config"


config:
  configure-commands:
  - |
    cat <<EOF >config.toml
    %{config-toml}
    EOF
  build-commands:
  - python3 x.py build -j${MAXJOBS}
  install-commands:
  - DESTDIR="%{install-root}" python3 x.py install
  - "%{install-extra}"
