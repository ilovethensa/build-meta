# Summary

<!-- Describe the feature that you want added -->

# Reasoning

<!--
Why & for whom is this feature useful?
Explain why you think it should it be in carbonOS?
-->

# Prior Art

<!--
If other Distros/OSs have this feature, please put links
to info about it here. Screenshots, links to existing
implementations, etc are all useful. This will help us
decide on how to best implement for this feature, if we
choose to do so
-->

/label ~proposal