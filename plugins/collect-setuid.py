"""
BuildStream cannot save the setuid status of files. This plugin
allows elements to list what files should be made setuid, and it
will be done at a later point (when an image is assembled).
"""

# Based on collect-initial-scripts, which was written for Freedesktop SDK:
# https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/blob/8a50ecc0/plugins/elements/collect_initial_scripts.py
# Then updated based on diff:
# https://gitlab.com/freedesktop-sdk/freedesktop-sdk/-/commit/043e7ece3748e32f4aee51bace73fda0fc17516d

import os
import re
from buildstream import Element

class ExtractSetuidElement(Element):

    BST_MIN_VERSION = "2.0"
    BST_FORBID_RDEPENDS = True
    BSD_FORBID_SOURCES = True

    def configure(self, node):
        node.validate_keys(['path'])

        path = node.get_str('path', '/setuid_files')
        path = path.lstrip(os.sep)
        self.dirname, self.filename = os.path.split(path)
        if not self.filename:
            raise ElementError("Expected a filename for path, got a directory")

    def preflight(self):
        pass

    def get_unique_key(self):
        return { 'dirname': self.dirname, 'filename': self.filename}

    def configure_sandbox(self, sandbox):
        pass

    def stage(self, sandbox):
        pass

    def assemble(self, sandbox):
        basedir = sandbox.get_virtual_directory()

        files = []
        for dependency in self.dependencies():
            public = dependency.get_public_data('setuid')
            if public and 'files' in public:                
                files += public.get_str_list('files')

        pathdir = basedir.open_directory(self.dirname, create=True)
        with pathdir.open_file(self.filename, mode='w') as f:
            f.write('\n'.join(files))
            os.chmod(f.fileno(), 0o644)

        return os.sep

def setup():
    return ExtractSetuidElement
